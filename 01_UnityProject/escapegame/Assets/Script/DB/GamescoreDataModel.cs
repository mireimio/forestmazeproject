﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;

public class GamescoreDataModel
{
    public static List<GamescoreData> DeserializeFromJson(string sStrJson)
    {
        var scoreDataList = new List<GamescoreData>();

        // JSONデータをDeserialize（デコード）し、リスト型にキャスト      
        IList jsonList = (IList)Json.Deserialize(sStrJson);

        // リストの内容はオブジェクト型なので、辞書型の変数に一つ一つ代入しながら処理
        foreach (IDictionary jsonOne in jsonList)
        {
            var scoredata = new GamescoreData();
            //レコード解析
            if (jsonOne.Contains("id"))
            {
                //オブジェクト型→long型→int型
                scoredata.id = System.Convert.ToInt32(jsonOne["id"]);
            }
            if (jsonOne.Contains("winplayer"))
            {
                //オブジェクト型→string型
                scoredata.winplayer = ((string)jsonOne["winplayer"]);
            }
            //リストに追加
            scoreDataList.Add(scoredata);
        }
        return scoreDataList;
    }
}
