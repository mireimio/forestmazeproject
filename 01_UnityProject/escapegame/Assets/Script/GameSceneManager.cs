﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
[RequireComponent(typeof(PhotonView))]

public class GameSceneManager : MonoBehaviour
{

    [SerializeField] public GameObject playerPrefab1;
    [SerializeField] public GameObject playerPrefab2;
    [SerializeField] public GameObject cameraPrefab1;
    [SerializeField] public GameObject cameraPrefab2;

    [SerializeField] public GameObject resultMessage;

    public List<PlayerScript> playerList = new List<PlayerScript>();


    void Awake()
    {
        //実体化
        GlobalGameManager._instance.gameSceneManager = this;
    }


    void Start()
    {
        //キャラ、カメラの生成
        InitPhotonObject();
    }


    void OnDisable()
    {
        Debug.Log("OnDisable");
    }
    void OnDestroy()
    {
        Debug.Log("OnDestroy");
        GlobalGameManager._instance.photonManager.debugMessage = null;
        GlobalGameManager._instance.gameSceneManager = null;
    }


    //キャラ、カメラの生成
    private void InitPhotonObject()
    {

        GameObject playerObj = null;
        GameObject camObj = null;

        //オーナーか否かで、異なるキャラ・カメラを生成する
        if (GlobalGameManager._instance.photonManager.isRoomMake)
        {
            //キャラ生成
            playerObj = PhotonNetwork.Instantiate(playerPrefab1.name, new Vector3(1.5f, -2.5f, 0f), Quaternion.identity, 0);
            //カメラ生成
            camObj = PhotonNetwork.Instantiate(cameraPrefab1.name, new Vector3(1.5f, -2.5f, -45f), Quaternion.identity, 0);
            //カメラにプレイヤーをアタッチ
            camObj.GetComponent<CameraController>().player = playerObj;
        }
        else
        {
            //キャラ生成
            playerObj = PhotonNetwork.Instantiate(playerPrefab2.name, new Vector3(34f, 4f, 0f), Quaternion.identity, 0);
            //カメラ生成
            camObj = PhotonNetwork.Instantiate(cameraPrefab2.name, new Vector3(34f, 4f, -45f), Quaternion.identity, 0);
            //カメラにプレイヤーをアタッチ
            camObj.GetComponent<CameraController>().player = playerObj;
        }
    }

    //脱出者(Escape)勝利イメージを出力
    public void SetEscapeWinResult()
    {
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
        resultMessage.GetComponent<Image>().sprite = Resources.Load<Sprite>("Escape");
    }

    //妨害者(Disturb)勝利イメージを出力
    public void SetDisturbWinResult()
    {
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
        resultMessage.GetComponent<Image>().sprite = Resources.Load<Sprite>("Disturb");
    }



    public void OnClickChangeMainScene()
    {
        //TODO 駄目だ出来ない
        
        //Menuシーンに遷移
        GlobalGameManager._instance.photonManager.SceneChangeMain();

        //ルーム閉じる
        GlobalGameManager._instance.photonManager.CloseRoom();

        //現在のRoomを退室して、マスターサーバーに戻る
        PhotonNetwork.LeaveRoom();

        //シーンを再起動
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("MenuScene");

    }

}
