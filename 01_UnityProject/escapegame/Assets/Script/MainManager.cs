﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainManager : MonoBehaviour
{
    [SerializeField] private Text DisplayField = default;
    [SerializeField] Button[] buttons = default;
    [SerializeField] Text roomName = null;
    private string logBuff = "";



    void Start()
    {
        //ルームはない状態！
        buttons[1].interactable = false;
        buttons[2].interactable = false;

        //押下したボタンを保持
        buttons[0].onClick.AddListener(OnButton01); //Photon接続
        buttons[1].onClick.AddListener(OnButton02); //ルーム作成
        buttons[2].onClick.AddListener(OnButton03); //ルームランダム入室
    }


    void Update()
    {
        //ロビーに入室した？
        if (GlobalGameManager._instance.photonManager.isJoinedLobby)
        {
            buttons[1].interactable = true;
            buttons[2].interactable = true;

        }
        //ルームに入室した？
        if (GlobalGameManager._instance.photonManager.isJoinedRoom)
        {
            buttons[1].interactable = false;
            buttons[2].interactable = false;

        }
    }

    void OnDisable()
    {
        Debug.Log("OnDisable");
    }
    void OnDestroy()
    {
        Debug.Log("OnDestroy");
        GlobalGameManager._instance.photonManager.debugMessage = null;
        GlobalGameManager._instance.mainManager = null;
    }



    //---------------------------------------------
    // ボタン押下時の挙動
    //---------------------------------------------

    /// photon接続
    public void OnButton01()
    {
        //Photon接続
        GlobalGameManager._instance.photonManager.ConnectPhoton();
        //接続できたら書き込む
        DisplayField.text = "Please [RoomCreate] or [InRoom] button";
        buttons[0].interactable = false;

        GlobalGameManager._instance.photonManager.debugMessage = debugMessage;
    }

    /// ルーム作成
    public void OnButton02()
    {
        //ルーム名の重複チェック
        if (GlobalGameManager._instance.photonManager.CheckRoomName(roomName.text))
        {
            //名前を付けて、ルームを作成
            GlobalGameManager._instance.photonManager.CreateRoom(roomName.text);
            //ルーム作成できたら、画面に出力
            DisplayField.text = string.Format("Waiting for participants", roomName.text);
        }
        else
        {
            //重複していたらエラー
            DisplayField.text = string.Format("Duplicate roomName！Please change the roomName.", roomName.text);
        }
    }


    /// ルーム入室（ランダム）
    public void OnButton03()
    {
        GlobalGameManager._instance.photonManager.JoinRandomRoom();
        DisplayField.text = "JoinRandomRoom";
    }


    /// photonのデバッグメッセージを取得するコールバック
    /// <param name="mess">Mess.</param>
    private void debugMessage(string mess)
    {
        logBuff += mess + "\n";
        DisplayField.text = logBuff;
    }

}
