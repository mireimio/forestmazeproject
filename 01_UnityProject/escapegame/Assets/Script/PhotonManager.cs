﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


//ルーム情報を保持するクラス
public class photonRoomInfo
{
    public string roomName;
    public string roomOwnerID;

    //コンストラクタ
    public photonRoomInfo()
    {
        roomName = "";
        roomOwnerID = "";
    }
}


public class PhotonManager : Photon.MonoBehaviour
{
    //Versionの指定
    private const string PHOTON_GAME_VER = "v1.0";
    //ルームのデフォルト名
    private const string GAMEROOM_NAME = "myroom01";
    //入室できる人数
    private const int GAMEROOM_LIMIT = 2;

    //photon初期化済みか？
    public bool isInitPhoton = false;
    //ロビー入場済みか？
    public bool isJoinedLobby = false;
    //ルーム入室済みか？
    public bool isJoinedRoom = false;
    //自分でルームを作成したのか？
    public bool isRoomMake = false;

    //今立ってるルームをリスト型で保持したい
    public List<photonRoomInfo> roomList = new List<photonRoomInfo>();

    //デバッグメッセージ表示用コールバック
    public Action<string> debugMessage = null;



    //コールバック関数で処理するための準備
    void Start()
    {
        //ゲームマネージャに自分自身の参照をセットする
        //※これにより、色々なクラスからゲームマネージャを通してこのphotonManagerにアクセスできる
        GlobalGameManager._instance.photonManager = this;
    }

    //Photon接続
    //ログイン処理
    public void ConnectPhoton()
    {
        Debug.Log("ConnectPhoton");

        //同時接続可能とする
        //これを利用することで、mastercliant が loadlevel を行うとき、クライアントもシーン遷移する。
        PhotonNetwork.automaticallySyncScene = true;
        //photonへの接続を行う
        PhotonNetwork.ConnectUsingSettings(PHOTON_GAME_VER);
        //1秒間に送信するパケット数を修正　
        //これを増やすと同期精度が上がるが通信負荷が増加する
        PhotonNetwork.sendRate = 60;
        PhotonNetwork.sendRateOnSerialize = 60;
    }

    //まずはランダムな部屋に入出する
    public void JoinRandomRoom()
    {
        Debug.Log("JoinRandomRoom");
        PhotonNetwork.JoinRandomRoom();
    }


    //ルーム名が重複してないか確認
    public bool CheckRoomName(string roomName , bool duplicateFlg = false)
    {
        photonRoomInfo info = null;
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();
        foreach (RoomInfo roomInf in rooms)
        {
            info = new photonRoomInfo();
            info.roomName = roomInf.CustomProperties["roomName"].ToString();
            if (info.roomName == roomName)
            {
                return duplicateFlg;
            }
        }
        return duplicateFlg = true;
    } 



    //ルーム作成　＆　他のメンバー入室待ち
    public void CreateRoom(string roomName = GAMEROOM_NAME)
    {
        Debug.Log(string.Format("CreateRoom{0}", roomName));

        // ユーザー情報（仮）
        string userId = "001";

        //カスタムプロパティ
        //ルームの基本情報を設定する準備
        ExitGames.Client.Photon.Hashtable customProp = new ExitGames.Client.Photon.Hashtable();
        customProp.Add("roomName", roomName);
        customProp.Add("roomOwnerID", userId);
        PhotonNetwork.SetPlayerCustomProperties(customProp);    // プレイヤーのプロパティをセットして、他プレイヤーにも同期するメソッド

        //ルームオプションにカスタムプロパティを設定
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.CustomRoomProperties = customProp;
        roomOptions.CustomRoomPropertiesForLobby = new string[] { "roomName", "roomOwnerID" };

        // 部屋の最大人数
        roomOptions.MaxPlayers = (byte)GAMEROOM_LIMIT;
        // 入室を許可する
        roomOptions.IsOpen = true;
        // ロビーから見えるようにする
        roomOptions.IsVisible = true;
        // userIdが名前のルームがなければ作って入室、あれば普通に入室する
        PhotonNetwork.CreateRoom(roomName, roomOptions, null);

        //自分がルームを作ったか？
        isRoomMake = true;

        // 対戦相手を待つ
        StartCoroutine(WaitOtherPlayer());
    }


    /// <summary>
	/// ルームを作成して他のプレイヤーが入ってくるのを待つ
	/// 人数がそろったらゲームシーンへ遷移
	/// </summary>
	public IEnumerator WaitOtherPlayer()
    {
        //誰か待ってるか？
        bool isWaiting = true;
        Debug.Log(string.Format("waiting start Limit:{0}", GAMEROOM_LIMIT));

        while (isWaiting)
        {
            //参加人数が2名になったら完了
            if ((PhotonNetwork.room != null) &&
                (PhotonNetwork.room.PlayerCount == GAMEROOM_LIMIT))
            {
                //人数がそろったのでfalseにする
                isWaiting = false;
            }
            yield return null;
        }

        Debug.Log("waiting finish!");

        //シーン遷移：ゲームシーンへ！
        SceneChangeGame();

        Debug.Log("ルームに入室しました");
    }


    //Photon版シーンチェンジ
    /// ゲームシーン
	public void SceneChangeGame()
    {
        Debug.Log("SceneChangeGame");

        //自分がMasterClientであるかどうか
        if (!PhotonNetwork.isMasterClient)
        {
            Debug.Log("Err:this function masterclient only!");
            return;
        }
        //シーン移動
        //ルーム内にいるユーザが同期された状態で遷移
        PhotonNetwork.LoadLevel("GameScene");
    }


    //Photon版シーンチェンジ
    /// メニューシーン
    public void SceneChangeMain()
    {
        Debug.Log("SceneChangeMain");

        if (!PhotonNetwork.isMasterClient)
        {
            Debug.Log("Err:this function masterclient only!");
            return;
        }
        //シーン移動
        PhotonNetwork.LoadLevel("MenuScene");
    }




    //---------------------------------------------
    //ルーム作成・接続する準備
    /// Photon.MonoBehaviourを継承
    //---------------------------------------------

    /// <summary>
    /// 既に存在しているルームに入室する
    /// </summary>
    /// <param name="roomName">Room name.</param>
    public void JoinRoom(string roomName = GAMEROOM_NAME)
    {
        Debug.Log(string.Format("JoinRoom({0})", roomName));
        isRoomMake = false;
        PhotonNetwork.JoinRoom(roomName);
    }

    /// <summary>
    /// Closes the room.
    /// </summary>
    /// <param name="isOpen">If set to <c>true</c> is open.</param>
    public void CloseRoom(bool isOpen = false)
    {
        Room room = PhotonNetwork.room;
        if (null == room)
        {
            return;
        }
        room.IsOpen = isOpen;
    }

    /// <summary>
    /// Gets the count by room.
    /// </summary>
    /// <returns>The count by room.</returns>
    public int GetCountByRoom()
    {
        Room room = PhotonNetwork.room;
        if (null == room)
        {
            return 0;
        }
        return room.PlayerCount;
    }

    /// <summary>
    /// photonルームリストを更新する
    /// </summary>
    public void CheckRoomList()
    {
        //リストをクリア
        roomList.Clear();
        photonRoomInfo info = null;

        //ルームリストを元にリストを更新
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();
        Debug.Log(string.Format("roomCnt[{0}]", rooms.Length));
        foreach (RoomInfo infoOne in rooms)
        {
            info = new photonRoomInfo();
            info.roomName = infoOne.CustomProperties["roomName"].ToString();
            info.roomOwnerID = infoOne.CustomProperties["roomOwnerID"].ToString();
            roomList.Add(info);
            //debug print
            Debug.Log(string.Format("roomName[{0}] roomOwnerID[{1}]", info.roomName, info.roomOwnerID));
        }
    }

    /// <summary>
    /// photonプレイヤーリストを調査する
    /// </summary>
    public void CheckPlayerList()
    {
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            Debug.Log(string.Format("ID:{0} UserId:{1} isMC:{2}",
                                    player.ID,
                                    player.UserId,
                                    player.IsMasterClient));
        }
    }




    //---------------------------------------------
    //photon利用するための基本処理
    /// Photon.MonoBehaviourを継承
    //---------------------------------------------
    // photon callback

    /// <summary>
    /// event:photonに接続した
    /// </summary>
    public void OnConnectedToPhoton()
    {
        Debug.Log("OnConnectedToPhoton");
        //photon初期化済み？
        isInitPhoton = true;
    }

    /// <summary>
    /// event:photonが切断した
    /// </summary>
    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("OnDisconnectedFromPhoton");
    }

    /// <summary>
    /// event:接続失敗
    /// </summary>
    public void OnConnectionFail()
    {
        Debug.Log("OnConnectionFail");
    }

    /// <summary>
    /// event:photon接続失敗
    /// </summary>
    /// <param name="parameters">Parameters.</param>
    public void OnFailedToConnectToPhoton(object parameters)
    {
        Debug.Log("OnFailedToConnectToPhoton");
    }

    /// <summary>
    /// event:ロビー入室
    /// </summary>
    public void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");
        isJoinedLobby = true;
    }

    /// <summary>
    /// event:ロビー退室
    /// </summary>
    public void OnLeftLobby()
    {
        Debug.Log("OnLeftLobby");
    }

    /// <summary>
    /// Raises the connected to master event.
    /// autoJoinLobby が true 時には OnJoinedLobby が代わりに呼ばれる。
    /// </summary>
    public void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster");
    }

    /// <summary>
    /// event:ルームリストが更新された
    /// </summary>
    public void OnReceivedRoomListUpdate()
    {
        Debug.Log("OnReceivedRoomListUpdate");
        //ルーム一覧を取得
        CheckRoomList();
    }

    /// <summary>
    /// event:ルーム作成
    /// </summary>
    public void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");
        Debug.Log(string.Format("Name:{0}", PhotonNetwork.room.Name));
    }

    /// <summary>
    /// event:ルーム作成失敗
    /// </summary>
    public void OnPhotonCreateRoomFailed()
    {
        Debug.Log("OnPhotonCreateRoomFailed");
    }

    /// <summary>
    /// event:ルーム入室
    /// </summary>
    public void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        Debug.Log(string.Format("Name:{0}", PhotonNetwork.room.Name));
        //ルーム入室済み？
        isJoinedRoom = true;
    }

    /// <summary>
    /// event:ルーム入室失敗
    /// </summary>
    /// <param name="cause">Cause.</param>
    public void OnPhotonJoinRoomFailed(object[] cause)
    {
        Debug.Log("OnPhotonJoinRoomFailed");
    }

    /// <summary>
    /// event:ランダム入室失敗
    /// </summary>
    public void OnPhotonRandomJoinFailed()
    {
        Debug.Log("OnPhotonRandomJoinFailed");
    }

    /// <summary>
    /// event:ルーム退室コールバック
    /// </summary>
    public void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");
    }
    /// <summary>
    /// event:誰かプレイヤーが接続された
    /// </summary>
    /// <param name="player">Player.</param>
    public void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerConnected");
    }

    /// <summary>
    /// event:誰かプレイヤーの接続が切れた
    /// </summary>
    /// <param name="player">Player.</param>
    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerDisconnected");
    }

    /// <summary>
    /// event:マスタークライアントが切り替わった
    /// </summary>
    /// <param name="player">Player.</param>
    public void OnMasterClientSwitched(PhotonPlayer player)
    {
        Debug.Log("OnMasterClientSwitched");
    }

}
