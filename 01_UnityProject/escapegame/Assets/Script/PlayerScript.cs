﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PhotonView))]

public class PlayerScript : MonoBehaviour
{
    [SerializeField] private PhotonView photonView = null;
    [SerializeField] private int photonId = 0;
    [SerializeField] public int photonOwnerId = 0;

    private string escape = "escape";
    private string disturb = "disturb";
    private string escTagName = "EscapePlayer";
    private string distTagName = "DisturbPlayer";
    private string exitTagName = "EscapeExit";
    private string itemTagName = "FlowerKeyItem";

    private bool escWinFlg = false;
    private bool distWinFlg = false;

    void Awake()
    {
        //自身のオブジェクトにアタッチされたphotonviewオブジェクト参照を取得する。
        photonView = this.GetComponent<PhotonView>();

        //デバック用
        if (null != photonView)
        {
            photonId = photonView.viewID;
            photonOwnerId = photonView.ownerId;
        }
    }


    void Start()
    {
        GlobalGameManager._instance.gameSceneManager.playerList.Add(this);
        Anim = GetComponent<Animator>();
    }



    private Animator Anim;

    void Update()
    {
        if (escWinFlg || distWinFlg)
        {
            //どちらかが勝利したら、フレームを止める
            Time.timeScale = 0;
        }
        else
        {
            //キャラ操作
            MoveCharacter();
        }
    }


    //キャラ操作
    private void MoveCharacter()
    {
        if ((null != photonView) && (photonView.isMine))
        {
            //自身の生成したオブジェクトの場合、上下左右操作
            //アニメーションの変数は、Windows＞Animation＞Animator＞Parametersから確認
            if (Input.GetKey(KeyCode.RightArrow))
            {
                Anim.SetFloat("rotationx", 1f);
                Anim.SetFloat("rotationy", 0f);
                transform.Translate(Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Anim.SetFloat("rotationx", -1f);
                Anim.SetFloat("rotationy", 0f);
                transform.Translate(-Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                Anim.SetFloat("rotationx", 0f);
                Anim.SetFloat("rotationy", 1f);
                transform.Translate(0, Time.deltaTime, 0);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                Anim.SetFloat("rotationx", 0f);
                Anim.SetFloat("rotationy", -1f);
                transform.Translate(0, -Time.deltaTime, 0);
            }
        }
    }


    //ゲーム終了条件
    void OnCollisionEnter2D(Collision2D col)
    {

        //脱出者勝利
        if (this.gameObject.CompareTag(escTagName))
        {
            if (col.gameObject.CompareTag(exitTagName) && null == GameObject.Find(itemTagName))
            {
                //FlowerKeyItemを取得した状態で、出口に触れた場合
                if ((null != photonView) && (photonView.isMine))
                {
                    escWinFlg = true;
                    //両画面に勝利イメージを出力
                    photonView.RPC("SetResultImg", PhotonTargets.All, escWinFlg, distWinFlg);
                }
            }
        }
        //妨害者勝利
        if (this.gameObject.CompareTag(distTagName))
        {
            //自身（DisturbPlayer）が、EscapePlayerに触れた場合
            if (col.gameObject.CompareTag(escTagName))
            {
                distWinFlg = true;
                //両画面に勝利イメージを出力
                photonView.RPC("SetResultImg", PhotonTargets.All, escWinFlg, distWinFlg);
            }
        }

        //勝利結果処理
        if ( escWinFlg || distWinFlg )
        {
            //DB登録は、ルーム作成者のみが行う
            if (GlobalGameManager._instance.photonManager.isRoomMake && escWinFlg)
            {
                //脱出者が勝者であることをDB登録する
                GlobalGameManager._instance.dateBaseManager.SetWinScoreData(escape);
            }
            else if (GlobalGameManager._instance.photonManager.isRoomMake && distWinFlg)
            {
                //妨害者が勝者であることをDB登録する
                GlobalGameManager._instance.dateBaseManager.SetWinScoreData(disturb);
            }

            //DBデータ取得＆勝率計算して画面出力
            photonView.RPC("GetScoreResult", PhotonTargets.AllViaServer);
        }

    }




    [PunRPC]
    //勝利イメージを出力
    public void SetResultImg(bool escWinFlg, bool distWinFlg)
    {
        if (escWinFlg)
        {
            GlobalGameManager._instance.gameSceneManager.SetEscapeWinResult();
        }
        else if(distWinFlg)
        {
            GlobalGameManager._instance.gameSceneManager.SetDisturbWinResult();
        }
    }

    [PunRPC]
    //DBデータ取得＆勝率計算して画面出力
    public void GetScoreResult()
    {
        GlobalGameManager._instance.dateBaseManager.GetWinScoreData();
    }



    /// パラメータの同期
	/// PhotonViewにこのスクリプトをアタッチすると OnPhotonSerializeView が定期的に呼ばれるようになる。
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {}


}