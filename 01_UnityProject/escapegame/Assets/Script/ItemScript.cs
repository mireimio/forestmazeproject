﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(PhotonView))]

///脱出ギミック用アイテム
public class ItemScript : MonoBehaviour
{

    //花(脱出アイテム)の取得
    void OnCollisionEnter2D(Collision2D col)
    {
        //花を取得(EscapePlayerのみ)
        if (col.gameObject.CompareTag("EscapePlayer"))
        {
            //this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
    }


    /// パラメータの同期
    /// PhotonViewにこのスクリプトをアタッチすると OnPhotonSerializeView が定期的に呼ばれるようになる。
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {}

}
