﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.Linq;

public class DataBaseManager : MonoBehaviour
{
    //結果出力エリア
    [SerializeField] public Text escWinPctText;
    [SerializeField] public Text distWinPctText;

    private float escWinCount = 0.0f;
    private float distWinCount = 0.0f;
    private float maxPlayCount = 0.0f;

    //レスポンス格納リスト
    private List<GamescoreData> scoreList = null;

    void Start()
    {
        GlobalGameManager._instance.dateBaseManager = this;

    }

    //ScoreDataデータをリクエストする
    public void SetWinScoreData(string winMenber)
    {
        Debug.Log(winMenber);
        //リクエスト
        StartCoroutine(DownloadJson(winMenber,CallbackWebrequestSuccess,CallbackWebrequestFailed));
    }


    //ScoreDataデータをリクエストする
    public void GetWinScoreData()
    {
        //リクエスト
        StartCoroutine(DownloadJson(OutputWebRequest, CallbackWebrequestFailed));
    }


    //DBから取得した結果をUIに出力
    private void OutputWebRequest(string response)
    {
        //デコードして、response(Json形式)をパラメータ郡(AddressDataModel形式)に戻す
        scoreList = GamescoreDataModel.DeserializeFromJson(response);

        //escape、disturbそれぞれ何個あるか数える
        foreach (GamescoreData scoredata in scoreList)
        {
            if ("escape" == scoredata.winplayer)
            {
                escWinCount = escWinCount +1.0f;
            }
            else if ("disturb" == scoredata.winplayer)
            {
                distWinCount = distWinCount + 1.0f;
            }
            maxPlayCount = maxPlayCount + 1.0f;
        }
        Debug.Log("総プレイ数" + maxPlayCount);
        Debug.Log("脱出者勝利数"+escWinCount);
        Debug.Log("妨害者勝利数" + distWinCount);

        //勝率出力 四捨五入（勝利数 ÷ 総プレイ数）
        escWinPctText.text = Convert.ToString("EscapeWin" + "\n" + Math.Round((escWinCount /maxPlayCount) * 100) + " %" );
        distWinPctText.text = Convert.ToString("DisturbWin" + "\n" + Math.Round((distWinCount / maxPlayCount) * 100) + " %" );
    }


    private void CallbackWebrequestSuccess(string response)
    {
        Debug.Log(response);
    }

    private void CallbackWebrequestFailed()
    {
        //jsonデータ取得に失敗
        Debug.Log("WebRequest Failed");
    }


    //Set
    private IEnumerator DownloadJson(string winMenber, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        //リクエスト(Post)する
        WWWForm form = new WWWForm();
        //idは自動採番のためセット不要
        form.AddField("winplayer", winMenber);
        UnityWebRequest webRequest = UnityWebRequest.Post("http://localhost:80/escapegame/gamescore/setGameScore/", form);

        //タイムアウト時間(秒)
        webRequest.timeout = 15;

        //リモートサーバとの通信を開始　URLに接続して、結果が戻ってくるまで待機
        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            // リクエスト成功の場合
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }


    //Get
    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        //リクエスト(GET)する
        UnityWebRequest webRequest = UnityWebRequest.Get("http://localhost:80/escapegame/gamescore/getGameScore/");
        yield return webRequest.SendWebRequest();
        if (webRequest.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            // リクエスト成功の場合
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }

}
