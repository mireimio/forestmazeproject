﻿public class GamescoreData
{
    public int id { get; set; }
    public string winplayer { get; set; }

    public GamescoreData()
    {
        id = 0;
        winplayer = "";
    }
}
