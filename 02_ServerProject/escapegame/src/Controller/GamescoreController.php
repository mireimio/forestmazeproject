<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Gamescore Controller
 *
 * @property \App\Model\Table\GamescoreTable $Gamescore
 *
 * @method \App\Model\Entity\Gamescore[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GamescoreController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $gamescore = $this->paginate($this->Gamescore);

        $this->set(compact('gamescore'));
    }

    /**
     * View method
     *
     * @param string|null $id Gamescore id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gamescore = $this->Gamescore->get($id, [
            'contain' => []
        ]);

        $this->set('gamescore', $gamescore);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gamescore = $this->Gamescore->newEntity();
        if ($this->request->is('post')) {
            $gamescore = $this->Gamescore->patchEntity($gamescore, $this->request->getData());
            if ($this->Gamescore->save($gamescore)) {
                $this->Flash->success(__('The gamescore has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gamescore could not be saved. Please, try again.'));
        }
        $this->set(compact('gamescore'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Gamescore id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gamescore = $this->Gamescore->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gamescore = $this->Gamescore->patchEntity($gamescore, $this->request->getData());
            if ($this->Gamescore->save($gamescore)) {
                $this->Flash->success(__('The gamescore has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gamescore could not be saved. Please, try again.'));
        }
        $this->set(compact('gamescore'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Gamescore id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gamescore = $this->Gamescore->get($id);
        if ($this->Gamescore->delete($gamescore)) {
            $this->Flash->success(__('The gamescore has been deleted.'));
        } else {
            $this->Flash->error(__('The gamescore could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //dataGet
    public function getGameScore(){
        $this->autoRender = false;
        $query = $this->Gamescore->find('all');
        $json_array = json_encode($query);
        echo $json_array;
    }


    //dataSet
    public function setGameScore(){
        $this->autoRender = false;
        $id = "";
        if( isset( $this->request->data['id']))
        {
            $id = $this->request->data['id'];
            error_log($id);
        }
        $winplayer = "";
        if( isset( $this->request->data['winplayer'] ) )
        {
            $winplayer = $this->request->data['winplayer'];
            error_log($winplayer);
        }

        $data = array ( 'id' => $id, 'winplayer' => $winplayer);
        $gamescore = $this->Gamescore->newEntity();
        $gamescore = $this->Gamescore->patchEntity($gamescore, $data);

        if ($this->Gamescore->save($gamescore))
        {
            echo "success!";
        }else{
            echo "failed!";
        }
    }
    
}
