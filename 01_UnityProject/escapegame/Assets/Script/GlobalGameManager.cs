﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// 全シーン共通のマネージャクラス
/// ※シーンが切り替わっても破棄されず内容が保持される。
public class GlobalGameManager : MonoBehaviour
{
    //シーンが切り替わっても、オブジェクトが破棄されないようにする（実体化）
    public static GlobalGameManager _instance = null;

    //Start関数前か、インスタンス化直後に実行される。
    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }
        //instanceが一つの場合、自身を代入する
        _instance = this;
        //このクラスがアタッチされているシーンは、シーンが切り替わっても破棄されないようになる
        DontDestroyOnLoad(gameObject);
    }

    //どのシーンからでもアクセス可能にする（実体化）
    public PhotonManager photonManager = null;
    public MainManager mainManager = null;
    public GameSceneManager gameSceneManager = null;
    public DataBaseManager dateBaseManager = null;

    void Start(){}
    void Update(){}

}
