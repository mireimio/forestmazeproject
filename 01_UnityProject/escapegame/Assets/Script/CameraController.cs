﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(PhotonView))]

public class CameraController : MonoBehaviour
{
    //プレイヤーゲームオブジェクトへの参照を格納
    public GameObject player;
    //プレイヤーとカメラ間のオフセット距離を格納
    private Vector3 offset;

    [SerializeField] private PhotonView photonView = null;
    [SerializeField] private int photonId = 0;
    [SerializeField] public int photonOwnerId = 0;


    void Awake()
    {
        //自身のオブジェクトにアタッチされたphotonviewオブジェクト参照を取得する。
        photonView = this.GetComponent<PhotonView>();
    }

    void Start()
    {
        if ((null != photonView) && (photonView.isMine))
        {
            //自身の生成したオブジェクトの場合、カメラとプレイヤーの距離の差を求める
            offset = transform.position - player.transform.position;
        }
        else
        {
            //自身の生成したオブジェクトじゃない場合
            Debug.Log("isMineがfalseだからカメラ非アクにする");
            this.gameObject.SetActive(false);
        }
    }


    void LateUpdate()
    {
        if ((null != photonView) && (photonView.isMine))
        {
            //自身の生成したオブジェクトの場合、プレイヤーから一定の距離をとってカメラ追尾する
            transform.position = player.transform.position + offset;
        }
    }

}
