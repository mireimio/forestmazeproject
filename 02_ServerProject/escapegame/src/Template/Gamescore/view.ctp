<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gamescore $gamescore
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Gamescore'), ['action' => 'edit', $gamescore->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Gamescore'), ['action' => 'delete', $gamescore->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gamescore->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Gamescore'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gamescore'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="gamescore view large-9 medium-8 columns content">
    <h3><?= h($gamescore->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Winplayer') ?></th>
            <td><?= h($gamescore->winplayer) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($gamescore->id) ?></td>
        </tr>
    </table>
</div>
