﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Panel開閉用スクリプト
public class PanelActionController : MonoBehaviour
{
    public GameObject panel;

    public void OnClickPanelOpen()
    {
        this.panel.SetActive(true);
    }

    public void OnClickPanelClose()
    {
        this.panel.SetActive(false);
    }

}
